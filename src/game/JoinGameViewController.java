/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import game.ws.GameDoesNotExists_Exception;
import game.ws.InvalidParameters_Exception;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author Edi
 */
public class JoinGameViewController implements Initializable {
    private SimpleBooleanProperty isBackPressed, finishedInit;
    
    @FXML
    private Button backButton, joinButton;
    @FXML
    private Pane gamesPane;
    @FXML
    private ComboBox<String> gamesCB;
    @FXML
    private TextField playerNameTextField;
    @FXML
    private Label errorMessageLabel;
    
    private String gameName, playerName;
    
    private List<String> waitingGames = null;
    private Thread poolThread;
    private int playerID;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        joinButton.setDisable(true);
        isBackPressed = new SimpleBooleanProperty(false);
        finishedInit = new SimpleBooleanProperty(false);
        playerNameTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                onPlayerNameChange();
            }
        });
    }    
    
    public void updateWithGames(){
        poolThread = new Thread(() -> {
            pullGames();
        });
        poolThread.start();
    }
    
    public void pullGames()
    {
            this.waitingGames = MainApp.gameWebService.getWaitingGames();
            Platform.runLater(()-> reloadGamesTable());
        try {
            Thread.sleep(20000);
        } catch (InterruptedException ex) {
            Logger.getLogger(JoinGameViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void reloadGamesTable(){
        gamesCB.getItems().clear();
        for (String gameName: waitingGames) {            
            gamesCB.getItems().add(gameName);
        }
    }
    
    public SimpleBooleanProperty getBackButtonPressed() {
        return isBackPressed;
    }
    
    @FXML
    protected void onBack(ActionEvent event) {
        if (poolThread != null) 
            poolThread.stop();
        isBackPressed.setValue(true);
        isBackPressed.setValue(false);
    }
    
    @FXML
    protected void onJoin(ActionEvent event) {
        if (gamesCB.getValue() == null) {
            setErrorMessage("Please choose a game");
            return;
        }
        if (poolThread != null) 
            poolThread.stop();
        try {
            playerID = MainApp.gameWebService.joinGame(gamesCB.getValue() , playerNameTextField.getText());
            gameName = gamesCB.getValue();
            finishedInit.setValue(true);
            finishedInit.setValue(false);
        } catch (GameDoesNotExists_Exception ex) {
            setErrorMessage(ex.getMessage());
        } catch (InvalidParameters_Exception ex) {
            setErrorMessage(ex.getMessage());
        }
        
    }
    
    public int getPlayerID(){
        return playerID;
    }
    
    public SimpleBooleanProperty getFinishedInit() {
        return finishedInit;
    }
    
    public String getGameName(){
        return gameName;
    }
    public String getPlayerName(){
        return playerNameTextField.getText();
    }
    
    @FXML
    protected void onPlayerNameChange() {
        updateContinueButtonState();
    }
    private void updateContinueButtonState() {
        boolean isEmptyName = getPlayerName().trim().isEmpty();
        joinButton.disableProperty().setValue(isEmptyName);
    }
    
    public void setErrorMessage(String message){
        this.errorMessageLabel.setText(message);
        errorMessageLabel.setVisible(true);
    }
}
