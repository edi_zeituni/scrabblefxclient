/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import game.model.ClientGame;
import game.model.Player;
import game.model.PlayersManager;
import game.view.components.PlayerStatusComponent;
import game.ws.DuplicateGameName_Exception;
import game.ws.GameDoesNotExists_Exception;
import game.ws.InvalidParameters_Exception;
import game.ws.InvalidXML_Exception;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Scanner;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;

/**
 * FXML Controller class
 *
 * @author danielo
 */
public class LoadGameViewController implements Initializable {

    @FXML
    private TextField filePath;
    
    @FXML
    private TextField playerNameField;
     
    @FXML
    private Button browseButton;
    
    @FXML
    private Button loadButton;
    
    @FXML
    private Button joinButton;
    
    @FXML
    private Button backButton;
    
    @FXML
    private Label messageLabel;
    
    @FXML
    private Pane playersPane;
    
//    private File file;
    private SimpleBooleanProperty isBackPressed = new SimpleBooleanProperty(false);
    private SimpleBooleanProperty isLoadDone = new SimpleBooleanProperty(false);
    private File file;
    private String gameName;
    private String playerName;
    private int playerID;

    @FXML
    protected void onBrowse(ActionEvent event){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Game");
        fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("XML", "*.xml"),
            new FileChooser.ExtensionFilter("All Files", "*.*")
        );
        file = fileChooser.showOpenDialog(MainApp.primaryStage);
        if(file != null)
            filePath.setText(file.getPath());
    }
    
    @FXML
    protected void onBack(ActionEvent event) {
        isBackPressed.setValue(true);
        isBackPressed.setValue(false);
    }
    
    @FXML
    protected void onLoad(ActionEvent event){
        Thread loadGameThread = createLoadGameThread();
        loadGameThread.start();
    }
    
    @FXML
    protected void onJoin(ActionEvent event){
        Thread joinGameThread = createjoinGameThread();
        joinGameThread.start();
    }

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        joinButton.disableProperty().bind(Bindings.equal(playerNameField.textProperty(),""));
        filePath.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                messageLabel.setOpacity(0);
                file = null;
            }
        });
    }    
    
    public SimpleBooleanProperty getBackButtonPressed() {
        return isBackPressed;
    }
    
    public SimpleBooleanProperty getLoadDone() {
        return isLoadDone;
    }
    
    private Thread createLoadGameThread() {
        Thread loadGameFromFile = new Thread(this::loadGameFromFile);
        loadGameFromFile.setDaemon(false);
        loadGameFromFile.setName("Load Game From File");
        return loadGameFromFile;
    }
    
    private Thread createjoinGameThread() {
        Thread joinGame = new Thread(this::joinGameThread);
        joinGame.setDaemon(false);
        joinGame.setName("Load Game From File");
        return joinGame;    
    }
    
    private void loadGameFromFile(){

        try {

            if(file == null && filePath.getText().equals("")) {
                updateUIMessage("Please enter a valid file name");
                return;
            }
            
            file = new File(filePath.getText());
            MainApp.savedGameFile = file;            
            String content = new Scanner(file).useDelimiter("\\Z").next();
            String gameName = MainApp.gameWebService.createGameFromXML(content);
            enablePlayerName();
            updateUIMessage("Game: '" + gameName + "' loaded successfully");
            disableLoadGame();
            this.gameName = gameName;
            
        } catch(FileNotFoundException ex) {
            updateUIMessage(ex.getMessage());
            file = null;
        } catch(DuplicateGameName_Exception | InvalidParameters_Exception | InvalidXML_Exception ex) {
            updateUIMessage(ex.getMessage());
            file = null;
        } finally {
            file = null;
        }
    }
    
    private void joinGameThread() {
        try {
            playerID = MainApp.gameWebService.joinGame(gameName , playerNameField.getText());
            playerName = playerNameField.getText();
            loadDone();
        } catch(GameDoesNotExists_Exception | InvalidParameters_Exception ex) {
            updateUIMessage(ex.getMessage());
        }
    }

    private void enablePlayerName() {
        Platform.runLater(()-> playerNameField.disableProperty().setValue(false));
    }
    
    private void loadDone() {
        Platform.runLater(() -> {
            isLoadDone.set(true);
            isLoadDone.set(false);
        });
    }
    
    private void updateUIMessage(String msg) {
        Platform.runLater(()-> {
            messageLabel.setOpacity(1);
            messageLabel.setText(msg);
        });
    }
    
    private void disableLoadGame() {
        Platform.runLater(()-> {
            loadButton.disableProperty().set(true);
            browseButton.disableProperty().set(true);
            filePath.disableProperty().set(true);
        });
    }
    
    public String getGameName(){
        return gameName;
    }
    
    public String getPlayerName(){
        return playerName;
    }
    
    public int getPlayerID() {
        return playerID;
    }
}

