/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;


import game.model.Board;
import game.model.Board.BoardException;
import game.model.Card;
import game.model.Cell;
import game.model.Cell.CellException;
import game.model.Deck;
import game.model.Deck.DeckException;
import game.model.ClientGame.GameException;
import game.model.Player;
import game.model.Player.PlayerException;
import game.model.PlayersManager;
import game.view.components.BoardComponent;
import game.view.components.CellComponent;
import game.view.components.PlayerStatusComponent;
import game.view.components.ReplaceJokerComponent;
import game.ws.InvalidParameters_Exception;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.ParallelTransition;
import javafx.animation.PathTransition;
import javafx.animation.RotateTransition;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import game.ws.Orientation;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author Edi
 */
public class GameViewController implements Initializable {
    @FXML
    private Pane playersPane;
    
    @FXML
    private Pane exchangePane;
    
    @FXML
    private GridPane exchangeCardsPane;
    
    @FXML
    private BoardComponent boardGridPane;
    
    @FXML
    private BorderPane mainBorderPane;
    
    @FXML
    private GridPane playerCardsPane;
    
    @FXML
    private Button resetButton;
    
    @FXML
    private Button sumbitButton;
    
    @FXML
    private Button passButton;
    
    @FXML
    private Button exchangeButton;
    
    @FXML
    private Label mainErrorLabel;
    
    PlayersManager playerManager;
    private SimpleBooleanProperty isExitPressed = new SimpleBooleanProperty(false);
    private SimpleBooleanProperty isGameOver = new SimpleBooleanProperty(false);
    private SimpleBooleanProperty isTimeouteReached = new SimpleBooleanProperty(false);
    private ArrayList<CellComponent> cardsOnBoard;
    private ArrayList<CellComponent> cardsOnExchangeBoard;
    private RotateTransition passButtonAnimation;
    private RotateTransition exchangeButtonAnimation;
    private PathTransition resetButtonAnimation;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            playerManager = MainApp.clientGame.getPlayersManager();
            int i = 0;
            initPlayersPane();
            initBoard();
            initCurrentPlayerCards();
            cardsOnExchangeBoard = new ArrayList<>();
            passButtonAnimation = createRotateAnimation(passButton);
            exchangeButtonAnimation = createRotateAnimation(exchangeButton);
            lockButtons(true);
        } catch (Cell.CellException ex) {
            Logger.getLogger(GameViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }   
        
    private void initPlayersPane(){
        for (Player player: playerManager.getPlayers()) {
            addPlayerToList(player);
        }
    }
    
    private void initBoard() throws Cell.CellException{
        boardGridPane = new BoardComponent(MainApp.clientGame.getBoard());
        cardsOnBoard = boardGridPane.getListOfChangedCells();
        boardGridPane.getCellListenToJoker().addListener(new ChangeListener<String>() { 
                    @Override
                    public void changed(ObservableValue<? extends String> observableValue, String oldValue, String newValue) {
                        if (!newValue.equals(""))
                          showReplaceJokerComponent(newValue);
                    }
                 });
        mainBorderPane.setCenter(boardGridPane);
    }
    
    private void initCurrentPlayerCards() {
        playerCardsPane.getChildren().removeAll(playerCardsPane.getChildren());
        Player clientPlayer = playerManager.getClientPlayer();
        String cardsString = clientPlayer.getCardsLetters();
        
        CellComponent newCell;
        int index = 0;
        for (int i = 0 ; index < cardsString.length(); i++ , index+=2) {
            newCell = new CellComponent("cardBackground",i, 0);
            newCell.setSize(60, 60);
            newCell.setCard(String.valueOf(cardsString.charAt(index)));
            newCell.setDragable(true);
            playerCardsPane.add(newCell, i, 0);
        }
    }
    
    private void addPlayerToList(Player player) {
        PlayerStatusComponent playerView = new PlayerStatusComponent(player.getName(), player.isHuman(),player.getScore());
        if (playerManager.getCurrentPlayer() != player)
            playerView.setOpacity(0.5);
        
        playersPane.getChildren().add(playerView);
    }
    
    @FXML
    protected void onExit(ActionEvent event) {
        isExitPressed.setValue(true);
        isExitPressed.setValue(false);
    }
    
    public void notifyGameIsOver(){
        isGameOver.setValue(true);
    }
    
    @FXML
    protected void onPass(ActionEvent event) {
        try {
            MainApp.clientGame.passTurn();
            reloadView(true);
        } catch (InvalidParameters_Exception ex) {
            showErrorWithMessage(ex.getMessage());
        }
    }
    
    private void passTurn(){
        hideErrorMessage();
        setPlayersOpacity();
        reloadView(true);
        //play();
    }
    
    @FXML
    protected void onReset(ActionEvent event){
        reloadView(false);
    }
    
    @FXML
    protected void onExchange(ActionEvent event){
        exchangeButton.setDisable(true);
        hideErrorMessage();
        cardsOnExchangeBoard.clear();
        exchangePane.setVisible(true);
        CellComponent newCell;
        for (int i = 0 ; i < 7; i++ ) {
            newCell = new CellComponent("cardBackground",0, i);
            newCell.setChangedList(cardsOnExchangeBoard);
            exchangeCardsPane.add(newCell, 0, i);
        }
    }
    
    @FXML
    protected void onFinalExchange(ActionEvent event){
        hideErrorMessage();
        String cards = "";
        if (!cardsOnExchangeBoard.isEmpty()) {
            String name;
            for (CellComponent cell: cardsOnExchangeBoard) {
                name = cell.getCardName();
                if (name.equals("joker")) 
                    name = " ";
                cards = cards + name;
            }
            
            try {
                lockButtons(true);
                MainApp.clientGame.replaceCards(cards);
                exchangePane.setVisible(false);
            } catch (InvalidParameters_Exception ex) {
                showErrorWithMessage(ex.getMessage());
            }
        }
    }
    
    @FXML
    protected void onSubmit(ActionEvent event) {
        lockButtons(true);
        Thread submitThread = createSubmitThread();
        submitThread.start();
        lockButtons(false);
    }
    
    private boolean submit(){
        if (cardsOnBoard.size() == 0) {
            return false;
        }
        hideErrorMessageInThread();
        try{
            Orientation orientation = getCellOrientetion();
            System.out.println("Orientation:" + orientation);
            sortCards(orientation);
            validateCardsPlacement(orientation);
            Cell wordStartCell = getStartCell(orientation);
            String word = getWord(orientation, wordStartCell);
            System.out.println(word);
            String lettersStartPos = Board.getBoardLocationFormat(cardsOnBoard.get(0).getXPos(), cardsOnBoard.get(0).getYPos());
            String wordStartPos = Board.getBoardLocationFormat(wordStartCell);
            String letters = getLettersOnBoard();
            MainApp.clientGame.MakeWord(lettersStartPos, orientation, letters, wordStartPos, orientation, word);
        } catch (InvalidParameters_Exception ex) {
            showErrorWithMessageInThread(ex.getMessage());
            return false;
        }
        catch(IOException ex) {
            showErrorWithMessageInThread("Cards not connected together");
            return false;
        }
        
        return true;
    }
    
    private Thread createSubmitThread() {
        Thread submitThread = new Thread(this::submit);
        submitThread.setDaemon(false);
        submitThread.setName("Submit Thread");
        return submitThread;
    }
    
    private void validateCardsPlacement(Orientation orientation) throws IOException{
        CellComponent currCellComp = cardsOnBoard.get(0);
        
        if(orientation == Orientation.VERTICAL) {
            for(int i=1; i < cardsOnBoard.size(); i++) {
                CellComponent tempCellComp = cardsOnBoard.get(i);
                if(currCellComp.getYPos() + 1 != tempCellComp.getYPos() || currCellComp.getXPos() != tempCellComp.getXPos()) {
                    throw new IOException();
                }
                currCellComp = tempCellComp;
            }
        }
        if(orientation == Orientation.HORIZONTAL) {
            for(int i=1; i < cardsOnBoard.size(); i++) {
                CellComponent tempCellComp = cardsOnBoard.get(i);
                if(currCellComp.getXPos() + 1 != tempCellComp.getXPos() || currCellComp.getYPos() != tempCellComp.getYPos()) {
                    throw new IOException();
                }
                currCellComp = tempCellComp;
            }
        }
    }
    
    private void showReplaceJokerComponent(String cellPos){
        ReplaceJokerComponent jokerReplaceComponent = new ReplaceJokerComponent(360 , boardGridPane.getHeight());
        jokerReplaceComponent.getReplacedCardProperty().addListener(new ChangeListener<String>() { 
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldValue, String newValue) {
               List<String> pos =  Arrays.asList(cellPos.split("\\s*,\\s*"));
               String col = pos.get(0);
               String row = pos.get(1);
               boardGridPane.getCellByPos(Integer.valueOf(col) , Integer.valueOf(row)).setCard(newValue);
               mainBorderPane.setCenter(boardGridPane);
            }
         });
        mainBorderPane.setCenter(jokerReplaceComponent);
    }
    
    private Orientation getCellOrientetion() throws IOException {
        Boolean seenHorizontalOrientation = false;
        Boolean seenVerticalOrientation = false;
        
        CellComponent currCell = cardsOnBoard.get(0);
        for(int i=1; i < cardsOnBoard.size(); i++) {
            if(currCell.getXPos() == cardsOnBoard.get(i).getXPos()) {
                seenVerticalOrientation = true;
                if(seenHorizontalOrientation) {
                    System.err.println("SEEN HORIZONTAL AND VERTICAL ORIENTATION");
                    throw new IOException();
                }
            }
            if(currCell.getYPos() == cardsOnBoard.get(i).getYPos()) {
                seenHorizontalOrientation = true;
                if(seenVerticalOrientation) {
                    System.err.println("SEEN HORIZONTAL AND VERTICAL ORIENTATION");
                    throw new IOException();    
                }
            }
            currCell = cardsOnBoard.get(i);
        }
        
        if(seenHorizontalOrientation)
            return Orientation.HORIZONTAL;
        if(seenVerticalOrientation)
            return Orientation.VERTICAL;
        
        //Ask baord for orientation
        return boardGridPane.getOrientationForSingleCardOnBoard();
    }
    
    private void sortCards(Orientation orientation) {
        if(orientation == Orientation.HORIZONTAL) {
            cardsOnBoard.sort(CellComponent.CellHorizontalComparator);
        }
        if(orientation == Orientation.VERTICAL) {
            cardsOnBoard.sort(CellComponent.CellVerticalComparator);
        }
    }
    
    private Cell getStartCell(Orientation orientation) {
        Board board = MainApp.clientGame.getBoard();
        CellComponent firstCellComponent = cardsOnBoard.get(0);
        Cell cellModel = null;
                        
        if(orientation == orientation.VERTICAL) {
            if(firstCellComponent.getYPos() - 1 >= 0) { 
                cellModel = board.getCell(firstCellComponent.getXPos(), firstCellComponent.getYPos() - 1);
                for(int i = cellModel.getY(); !cellModel.isEmpty() && i >= 0; i--) {
                    Cell cellModelTemp = board.getCell(cellModel.getX(), i);
                    if(cellModelTemp.isEmpty()){
                        break;
                    }
                    cellModel = cellModelTemp;
                }
            } else {
                cellModel = board.getCell(firstCellComponent.getXPos(), firstCellComponent.getYPos());
            }
        }
        if(orientation == orientation.HORIZONTAL) {
            if(firstCellComponent.getXPos() - 1 >= 0) { 
                cellModel = board.getCell(firstCellComponent.getXPos() - 1, firstCellComponent.getYPos());
                for(int i = cellModel.getX() - 1; !cellModel.isEmpty() && i >= 0; i--) {
                    Cell cellModelTemp = board.getCell(i, cellModel.getY());
                    if(cellModelTemp.isEmpty()){
                        break;
                    }
                    cellModel = cellModelTemp;
                } 
            } else {
               cellModel = board.getCell(firstCellComponent.getXPos(), firstCellComponent.getXPos());
            }
        }
        if(cellModel.isEmpty()) {
            Cell cell = new Cell(firstCellComponent.getXPos(), firstCellComponent.getYPos(), null);
            cell.setCard(new Card(firstCellComponent.getCardName().charAt(0)));
            return cell;
        }
        return cellModel;
    }
    
    private String getWord(Orientation orientation, Cell startCell) {
        Board board = MainApp.clientGame.getBoard();
        Cell currCell = startCell;
        String word = "";
        int letterOnBoardCounter = 0;
        
        if(cardsOnBoard.get(0).getXPos() == startCell.getX() && cardsOnBoard.get(0).getYPos() == startCell.getY()) {
            letterOnBoardCounter++;
        }
            
        if(orientation == Orientation.VERTICAL) {
            int i = startCell.getY() + 1;
            
            while(true) {
                if(!currCell.isEmpty()){
                    try {
                        word += currCell.getCard().getLetter();
                    } catch(CellException ex) {
                    
                    }
                } else {
                    if(letterOnBoardCounter < cardsOnBoard.size()) {
                        CellComponent cellComp = cardsOnBoard.get(letterOnBoardCounter);
                        word += cellComp.getCardName();
                        letterOnBoardCounter++;
                    } else {
                        break;
                    }
                }
                if(i < Board.ROW) {
                    currCell = board.getCell(currCell.getX(), i);
                } else {
                    break;
                }
                i++;
            }
        }
        
        if(orientation == Orientation.HORIZONTAL) {
            int i = startCell.getX() + 1;
            
            while(true) {
                if(!currCell.isEmpty()){
                    try {
                        word += currCell.getCard().getLetter();
                    } catch(CellException ex) {
                    
                    }
                } else {
                    if(letterOnBoardCounter < cardsOnBoard.size()) {
                        CellComponent cellComp = cardsOnBoard.get(letterOnBoardCounter);
                        word += cellComp.getCardName();
                        letterOnBoardCounter++;
                    } else {
                        break;
                    }
                }
                if(i < Board.COL) {
                    currCell = board.getCell(i, currCell.getY());
                } else {
                    break;
                }
                i++;
            }
        }
        return word;
    }
    
    private String getLettersOnBoard(){
        String letters = "";
        
        for(CellComponent cell : cardsOnBoard) {
            letters += cell.getCardName();
        }
        
        return letters;
    }
    
    private void setPlayersOpacity() {
        String currentPlayerName = playerManager.getCurrentPlayer().getName();
        for(Node node : playersPane.getChildren()) {
            PlayerStatusComponent playerView = (PlayerStatusComponent)node;
            Player player = playerManager.getPlayerByName(playerView.getName());
            playerView.setScore(player.getScore());
            if(!currentPlayerName.equals(playerView.getName())) {
                    playerView.setOpacity(0.5);
            } else {
                playerView.setOpacity(1);
            }
        }
    }   
    
    public SimpleBooleanProperty getExitButtonPressed() {
        return isExitPressed;
    }
    
    public SimpleBooleanProperty getIsGameOver() {
        return isGameOver;
    }
    
    public SimpleBooleanProperty getIsTimeoutReached() {
        return isTimeouteReached;
    }
    private void showErrorWithMessage(String message){
        mainErrorLabel.setText(message);
        mainErrorLabel.setVisible(true);
    }
    
    private void hideErrorMessage(){
        mainErrorLabel.setVisible(false);
    }
    
    private void hideErrorMessageInThread() {
        Platform.runLater(()->hideErrorMessage());
    }
    
    private void reloadView(Boolean shouldLockButtons){
        hideErrorMessage();
        mainBorderPane.setCenter(boardGridPane);
        exchangePane.setVisible(false);
        initCurrentPlayerCards();
        boardGridPane.removeUncomittedCardsFromBoard();
        lockButtons(shouldLockButtons);
    }
    
//    private void play(){
//        try {
//            Thread.sleep(500);
//        } catch (InterruptedException ex) {
//            Logger.getLogger(GameViewController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        if (!playerManager.getCurrentPlayer().isHuman()){
//                lockButtons(true);
//                computerPlay();
//        }
//    }
    
    private void computerPlay(){
        try {
            Thread.sleep(500);
        } catch (InterruptedException ex) {
            Logger.getLogger(GameViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //Computer Turn
        // choose 1word/2change/3pass
        Random randIndex = new Random();
        int newI = randIndex.nextInt(3);
        switch (newI){
            case 0:
                //makeword
                computerMakeWord();
                break;
            case 1:
                //exchange
                computerExchange();
                break;
            default:
                passWithAnimation();
                break;
        }
    }
    
    private void passWithAnimation(){
        passButtonAnimation.play();
    }
    
    private void animationMoveNodeAndSubmit(CellComponent node, int toX, int toY){
        TranslateTransition transition = 
                new TranslateTransition(Duration.seconds(0.7), node);
        transition.setOnFinished((new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if(!submit())
                    passTurnInThread();
            }
        }));
        transition.setFromX(0);
        transition.setFromY(0);
        transition.setToX(toX);
        transition.setToY(toY);
        transition.setAutoReverse(true);
        transition.play();
    }
    
    private void animationSubmitGesture(){
        ParallelTransition parallelTransition = new ParallelTransition();
        for (CellComponent cell: cardsOnBoard) 
            parallelTransition.getChildren().add(createRotateAnimation(cell));
        parallelTransition.play();
    }
    
    private void animationSubmitGestureInThread() {
        Platform.runLater(()->animationSubmitGesture());
    }
    
    public void commitCardsToBoard() {
        Platform.runLater(()-> boardGridPane.commitCardsToBoard());
    }
    
    public void removeUncomittedCardsFromBoard() {
        Platform.runLater(()->boardGridPane.removeUncomittedCardsFromBoard());
    }
    
    private void passTurnInThread() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(GameViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Platform.runLater(()-> passTurn());
    }
    
    private void showErrorWithMessageInThread(String text) {
        Platform.runLater(()-> showErrorWithMessage(text));
    }
    
    private void computerExchange(){
//        try {
//            MainApp.clientGame.computerChangeCard(playerManager.getCurrentPlayer());
//            initCurrentPlayerCards();
//        } catch (PlayerException ex) {
//            mainErrorLabel.setText(ex.text);
//        } catch (DeckException ex) {
//            mainErrorLabel.setText(ex.text);
//        }
//        exchangeButtonAnimation.setOnFinished((new EventHandler<ActionEvent>() {
//            @Override
//            public void handle(ActionEvent t) {
//                passTurn();
//            }
//        }));
//        exchangeButtonAnimation.play();
    }
    
    private void computerMakeWord(){
//        String emptyCell = MainApp.clientGame.getEmptyCellForComputer();
//        if (emptyCell.equals("") ||emptyCell.equals("7,7") ) {
//            computerPass();
//            return;
//        }
//        List<String> pos =  Arrays.asList(emptyCell.split("\\s*,\\s*"));
//        String col = pos.get(0);
//        String row = pos.get(1);
//        
//        Random randIndex = new Random();
//        int newI = randIndex.nextInt(7);
//        CellComponent cardComp = (CellComponent) playerCardsPane.getChildren().get(newI);
//        if (cardComp.getCardName().equals("joker")) {
//            computerExchange();
//            return;
//        }
//        CellComponent emptyCellComp = boardGridPane.getCellByPos(Integer.valueOf(col) , Integer.valueOf(row));
//        cardsOnBoard.add(emptyCellComp);
//        emptyCellComp.setCard(cardComp.getCardName());
//        animationMoveNodeAndSubmit(cardComp, -20, -100 );
    }
        
    private RotateTransition createRotateAnimation(Node node){
        RotateTransition trans = 
                new RotateTransition(Duration.seconds(1.0), node );
        trans.setFromAngle(0);
        trans.setToAngle(360);
        return trans;
    }
    
    private void lockButtons(Boolean isLock){
        passButton.setDisable(isLock);
        exchangeButton.setDisable(isLock);
        resetButton.setDisable(isLock);
        sumbitButton.setDisable(isLock);
        playerCardsPane.setDisable(isLock);
    }
    
    public void getActionFromPlayer(int timeOut){
        Platform.runLater(()->notifyUserToTakeAction());
    }
    
    private void notifyUserToTakeAction(){
        lockButtons(false);
    }
    
    public void changeCurrentPlayer(){
        Platform.runLater(()->passTurn());
    }
    
    public void notifyPassAction(){
        Platform.runLater(()->passWithAnimation());
    }
    
    public void updateClientPlayerCards(String letters){
        Player clientPlayer = playerManager.getClientPlayer();
        clientPlayer.setLetters(letters);
        Platform.runLater(()->initCurrentPlayerCards());
    }

    public void updateBoardWithNewLetters(String lettersStartPos, Orientation lettersOrientation, String letters)
    {
        //because we want to perform the change on fx thread first we need to set the 
        // values (lettersStartPos, lettersOrientation, letters) inside the board view component,
        // then the function changeLettersOnBoardWithLaterValues uses them inside fx thread and clean the values at the end
        boardGridPane.setValuesForLaterChangeCells(lettersStartPos, lettersOrientation, letters);
        Platform.runLater(()-> boardGridPane.changeLettersOnBoardWithLaterValues());
        animationSubmitGestureInThread();
        Platform.runLater(()->commitCardsToBoard());
    }
    
    public void notifyGameTimout() {
        isTimeouteReached.setValue(true);
    }
}