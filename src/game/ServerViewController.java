/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.io.File;
import java.io.IOException;
 
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
 
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
 

public class ServerViewController implements Initializable {
    @FXML
    private TextField addressField, portField;
    
    @FXML
    private Label status;
    
    @FXML
    private Button connectButton;
    
    private SimpleBooleanProperty connectPressed = new SimpleBooleanProperty(false);
    private final String  lastServerSettingFile = "src/game/view/utils/lastServerSettings.xml";
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        connectButton.disableProperty().bind(Bindings.or(Bindings.equal(addressField.textProperty(), ""), Bindings.equal(portField.textProperty(), "")));
        addressField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                status.visibleProperty().set(false);
            }
        });
        portField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                status.visibleProperty().set(false);
            }
        });
        loadLastServerSettings();
    }    
    
    @FXML
    private void onConnect(ActionEvent event) {
        status.visibleProperty().set(true);
        status.setText("Connecting to server...");
        saveLastServerSettings();
        connectPressed.set(true);
        connectPressed.set(false);
    }
    
    public void connectFailed() {
        status.setText("Connection failed");
    }
    
    public SimpleBooleanProperty getConnectPressed() {
        return connectPressed;
    }
    
    public String getAddress(){
        return addressField.getText();
    }
    
    public String getPort() {
        return portField.getText();
    }
    
    private void loadLastServerSettings() {
        File fXmlFile = new File(lastServerSettingFile);
        try {
	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	Document doc = dBuilder.parse(fXmlFile);
        
        doc.getDocumentElement().normalize();
        
        Element rootElement = doc.getDocumentElement();
        addressField.setText(rootElement.getAttribute("IP"));
        portField.setText(rootElement.getAttribute("PORT"));
        
        } catch(IOException | ParserConfigurationException | SAXException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    private void saveLastServerSettings() {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            
            Document doc = docBuilder.newDocument();
            Element mainRootElement = doc.createElement("ServerSettings");
            
            doc.appendChild(mainRootElement);
            
            Attr ipAttr = doc.createAttribute("IP");
            ipAttr.setValue(addressField.getText());
            mainRootElement.setAttributeNode(ipAttr);
            
            Attr portAttr = doc.createAttribute("PORT");
            portAttr.setValue(portField.getText());
            mainRootElement.setAttributeNode(portAttr);
            
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(lastServerSettingFile));
            
            transformer.transform(source, result);
            
        } catch(TransformerException | DOMException | ParserConfigurationException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
