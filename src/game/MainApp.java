/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;


import game.model.ClientGame;
import game.model.Player;
import game.model.PlayersManager;
import game.view.components.ServerTaskComponent;
import game.ws.GameDoesNotExists_Exception;
import game.ws.InvalidParameters_Exception;
import game.ws.ScrabbleWebService;
import game.ws.ScrabbleWebServiceService;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;


/**
 *
 * @author Edi
 */
public class MainApp extends Application {
    private static final String SERVER_VIEW_FXML_PATH = "view/ServerView.fxml";
    private static final String ADD_PLAYERS_FXML_PATH = "view/AddPlayersView.fxml";
    private static final String ROOT_LAYOUT_FXML_PATH = "view/RootLayout.fxml";
    private static final String WELCOME_VIEW_FXML_PATH = "view/WelcomeView.fxml";
    private static final String LOAD_GAME_VIEW_FXML_PATH = "view/LoadGameView.fxml";
    private static final String GAME_VIEW_FXML_PATH = "view/GameView.fxml";
    private static final String EXIT_VIEW_FXML_PATH = "view/ExitView.fxml";
    private static final String GAME_OVER_VIEW_FXML_PATH = "view/GameOverView.fxml";
    private static final String GAME_TIMEOUT_VIEW_FXML_PATH = "view/GameTimeOutView.fxml";
    private static final String WAITING_FXML_PATH = "view/WaitingView.fxml";
    private static final String JOIN_GAME_FXML_PATH = "view/JoinGameView.fxml";
    
    public static File savedGameFile;
    public static ScrabbleWebServiceService service;
    public static ScrabbleWebService gameWebService;
    private Scene MainMenuScene;
    private Scene ServerScens;
    private Scene GameScene;
    private Stage ExitStage;
    private Scene GameOverScene;
    private Scene GameTimoutScene;
    protected static Stage primaryStage; 
    private PlayersManager playersManager;
    private int playerID;
    private WelcomeViewController welcomeController;
    private WaitingViewController waitController;
    public static GameViewController gameController;
    public static ClientGame clientGame;
    
    @Override
    public void start(Stage stage) throws Exception {  
        clientGame = new ClientGame();
        MainApp.primaryStage = stage;
        MainApp.savedGameFile = null;
        this.playersManager = clientGame.getPlayersManager();
        
//        gameWebService.createGame("daniel", 2, 0);
//        int id1 = gameWebService.joinGame("daniel", "offek");
//        int id2 = gameWebService.joinGame("daniel", "offek1");
        
        serverMenuInit();
        showServerMenu();
        
    }
    
    public void serverMenuInit() {
        try {
            FXMLLoader fxmlLoader = getFXMLLoader(SERVER_VIEW_FXML_PATH);
            Parent serverRoot = getRoot(fxmlLoader);

            ServerViewController serverViewController = getServerViewController(fxmlLoader, primaryStage);

            ServerScens = new Scene(serverRoot, 300 , 248);
        } catch (IOException ex) {
            Logger.getLogger(MainApp.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    
    public void showServerMenu() {
        primaryStage.setTitle("Scrabble Server Menu");
        primaryStage.setScene(ServerScens);
        primaryStage.show();
    }
    
    public void mainMenuInit(){
        try {
            FXMLLoader fxmlLoader = getFXMLLoader(WELCOME_VIEW_FXML_PATH);
            Parent welcomeRoot = getRoot(fxmlLoader);

            welcomeController = getWelcomeViewController(fxmlLoader, primaryStage);

            MainMenuScene = new Scene(welcomeRoot, 600 , 400);
        } catch (IOException ex) {
            Logger.getLogger(MainApp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void showMainMenu(){
        primaryStage.setTitle("Scrabble");
        primaryStage.setScene(MainMenuScene);
        primaryStage.show();
    }
    
    public void showGameView() throws IOException, InvalidParameters_Exception, GameDoesNotExists_Exception, PlayersManager.PlayerManagerException, Player.PlayerException{
        clientGame.setPlayerId(playerID);
        clientGame.initGameByPullEvents();
        FXMLLoader fxmlLoader = getFXMLLoader(GAME_VIEW_FXML_PATH);
        Parent gameRoot = getRoot(fxmlLoader);
        gameController = getGameViewController(fxmlLoader, primaryStage);
        Scene gameView = new Scene(gameRoot, 900 , 600);
        primaryStage.setTitle("Scrabble");
        primaryStage.setScene(gameView);
        primaryStage.show();
        clientGame.startGame(gameController);
    }
    
    private void initExitDialog() {
        try {
            Stage dialogStage = new Stage();
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            
            FXMLLoader fxmlLoader = getFXMLLoader(EXIT_VIEW_FXML_PATH);
            Parent exitRoot = getRoot(fxmlLoader);
            ExitViewController exitViewControllerget = getExitViewController(fxmlLoader, dialogStage);
            
            dialogStage.setTitle("Exit");
            dialogStage.setScene(new Scene(exitRoot));
            ExitStage = dialogStage;
        } catch (IOException ex) {
            Logger.getLogger(MainApp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void showExitDialog(){
        ExitStage.show();
    }
    
    private void closeExitDialog(){
        ExitStage.close();
    }
    
    private void initGameOverScene() throws IOException{
        FXMLLoader fxmlLoader = getFXMLLoader(GAME_OVER_VIEW_FXML_PATH);
        Parent gameRoot = getRoot(fxmlLoader);
        GameOverViewController gameOver = getGameOverViewController(fxmlLoader, primaryStage);
        Scene gameOverView = new Scene(gameRoot);
        this.GameOverScene = gameOverView;
    }
    
    private void showGameOverScene(){
        primaryStage.setTitle("Game Over");
        primaryStage.setScene(GameOverScene);
        primaryStage.show();  
    }
    
    private void initTimeoutScene() throws IOException {
        FXMLLoader fxmlLoader = getFXMLLoader(GAME_TIMEOUT_VIEW_FXML_PATH);
        Parent gameRoot = getRoot(fxmlLoader);
        GameTimeOutViewController gameTimeOutController = getGameTimeOuteViewController(fxmlLoader, primaryStage);
        Scene gameTimeOutView = new Scene(gameRoot);
        this.GameTimoutScene = gameTimeOutView;
    }
    
    private void showTimeoutScene(){
        primaryStage.setTitle("Game Timoute");
        primaryStage.setScene(GameTimoutScene);
        primaryStage.show();  
    }
    
    private Parent getRoot(FXMLLoader fxmlLoader) throws IOException {
        URL url = fxmlLoader.getLocation();
        InputStream is = url.openStream();
        Parent p = (Parent) fxmlLoader.load(is);
        return p;
    }
    
    private ServerViewController getServerViewController(FXMLLoader fxmlLoader, final Stage primaryStage)  {
        ServerViewController serverViewController = (ServerViewController)fxmlLoader.getController();
        serverViewController.getConnectPressed().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> source, Boolean oldValue, Boolean newValue) {
                ServerTaskComponent serverTask = new ServerTaskComponent();
                
                if (newValue) {
                    try {
                        String address = serverViewController.getAddress();
                        String port = serverViewController.getPort();
                        URL location = new URL("http://" + address + ":" + port + "/scrabble/ScrabbleWebServiceService?wsdl");
                        
                        serverTask.connectServerTask(location);
                        service = new ScrabbleWebServiceService();
                        gameWebService = service.getScrabbleWebServicePort();
                        mainMenuInit();
                        showMainMenu();
                    } catch(MalformedURLException | ExecutionException | InterruptedException | TimeoutException ex) {
                        serverTask.cancelServerTask();
                        serverViewController.connectFailed();
                    }
                }
            }
        });
        
        return serverViewController;
    }
    
    private WelcomeViewController getWelcomeViewController(FXMLLoader fxmlLoader, final Stage primaryStage) {
        WelcomeViewController welcomeViewController = (WelcomeViewController) fxmlLoader.getController();
        welcomeViewController.getNewGameSelected().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> source, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    FXMLLoader fxmlLoader = getFXMLLoader(ADD_PLAYERS_FXML_PATH);
                    Parent playersRoot = null;
                    try {
                        playersRoot = getRoot(fxmlLoader);
                    } catch (IOException ex) {
                        Logger.getLogger(MainApp.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    AddPlayersViewController addPlayersController = getAddPlayersController(fxmlLoader, primaryStage);

                    Scene scene = new Scene(playersRoot, 600, 400);

                    primaryStage.setTitle("Players Manager");
                    primaryStage.setScene(scene);
                    primaryStage.show();
                }
            }
        });
        welcomeViewController.getLoadGameSelected().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> source, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    FXMLLoader fxmlLoader = getFXMLLoader(LOAD_GAME_VIEW_FXML_PATH);
                    
                    try {
                        Parent loadGameRoot = getRoot(fxmlLoader);
                        getLoadGameView(fxmlLoader, primaryStage);
                        Scene scene = new Scene(loadGameRoot, 600, 400);
                        primaryStage.setTitle("Load Game");
                        primaryStage.setScene(scene);
                        primaryStage.show();
                        
                    } catch (IOException ex) {
                        Logger.getLogger(MainApp.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
        welcomeViewController.getJoinGameSelected().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> source, Boolean oldValue, Boolean newValue) {
//                if (newValue) {
//                    playerID = welcomeController.getPlayerID();
//                    MainApp.clientGame.setGameName(welcomeController.getGameName());
//                    MainApp.clientGame.setPlayerId(playerID);
//                    MainApp.clientGame.setPlayerName(welcomeController.getPlayerName());
//                    showWaitingView();
//                }
                if (newValue) {
                    showJoinView();
                }
            }
        });
        return welcomeViewController;
    }
    
    private AddPlayersViewController getAddPlayersController(FXMLLoader fxmlLoader, final Stage primaryStage) {
        AddPlayersViewController playersController = (AddPlayersViewController) fxmlLoader.getController();
        playersController.getFinishedInit().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> source, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    try {
                        MainApp.clientGame.setGameName(playersController.getGameName());
                        playerID = MainApp.gameWebService.joinGame(playersController.getGameName(), playersController.getPlayerName());
                        MainApp.clientGame.setPlayerId(playerID);
                        MainApp.clientGame.setPlayerName(playersController.getPlayerName());
                        showWaitingView();
                    } catch (GameDoesNotExists_Exception ex) {
                        playersController.setErrorMessage(ex.getMessage());
                    } catch (InvalidParameters_Exception ex) {
                        playersController.setErrorMessage(ex.getMessage());
                    }
                }
            }
        });
        playersController.getBackButtonPressed().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> source, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    showMainMenu();
                }
            }
        });
        return playersController;
    }
    
    private LoadGameViewController getLoadGameView(FXMLLoader fxmlLoader, final Stage primaryStage) {
        LoadGameViewController loadGameViewController = (LoadGameViewController) fxmlLoader.getController();
        loadGameViewController.getLoadDone().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> source, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    MainApp.clientGame.setGameName(loadGameViewController.getGameName());
                    MainApp.clientGame.setPlayerId(loadGameViewController.getPlayerID());
                    MainApp.clientGame.setPlayerName(loadGameViewController.getPlayerName());
                    showWaitingView();
                }
            }
        });
        loadGameViewController.getBackButtonPressed().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> source, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    showMainMenu();
                }
            }
        });
        return loadGameViewController;
    }
    
    private GameViewController getGameViewController(FXMLLoader fxmlLoader, final Stage primaryStage) {
        GameViewController gameViewController = (GameViewController) fxmlLoader.getController();
//        initExitDialog();
//        gameViewController.getExitButtonPressed().addListener(new ChangeListener<Boolean>() {
//            @Override
//            public void changed(ObservableValue<? extends Boolean> source, Boolean oldValue, Boolean newValue) {
//                if (newValue) {
//                    showExitDialog();
//                }
//            }
//        });
        gameViewController.getIsGameOver().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> source, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    System.out.println("*************GAME OVER***************");
                    try {
                        initGameOverScene();
                        showGameOverScene();
                    } catch(IOException ex) {
                        System.out.println("Game Over Scene Faild To Load");
                    }
                }
            }
        });
        gameViewController.getIsTimeoutReached().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> source, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    try {
                        initTimeoutScene();
                        showTimeoutScene();
                    } catch(IOException ex) {
                        System.out.println("Error loading game timout scene");
                    }
                }
            }
        });
        return gameViewController;
    }
    
    private ExitViewController getExitViewController(FXMLLoader fxmlLoader, final Stage primaryStage) {
        ExitViewController exitViewController = (ExitViewController) fxmlLoader.getController();
        exitViewController.getBackButtonPressed().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> source, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    closeExitDialog();
                }
            }
        });
        return exitViewController;
    }
    
    private GameOverViewController getGameOverViewController(FXMLLoader fxmlLoader, final Stage primaryStage) {
        GameOverViewController gameOverViewController = (GameOverViewController) fxmlLoader.getController();
        gameOverViewController.getMainMenuPressed().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> source, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    showMainMenu();
                }
            }
        });
        //TODO maybe remove the option
//        gameOverViewController.getRestertPressed().addListener(new ChangeListener<Boolean>() {
//            @Override
//            public void changed(ObservableValue<? extends Boolean> source, Boolean oldValue, Boolean newValue) {
//                if (newValue) {
//                    try {
//                        game.resetGame();
//                        showGameView();
//                    } catch(Player.PlayerException ex) {
//                        System.out.println("Error Restarting Game");
//                        System.exit(1);
//                    } catch(Deck.DeckException ex) {
//                        System.out.println("Error Restarting Game");
//                        System.exit(1);
//                    } catch(IOException ex) {
//                        System.out.println("Error Restarting Game");
//                        System.exit(1);
//                    }
//                }
//            }
//        });
        gameOverViewController.getExitPressed().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> source, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    System.exit(0);
                }
            }
        });
        
        return gameOverViewController;
    }
    
    private GameTimeOutViewController getGameTimeOuteViewController(FXMLLoader fxmlLoader, final Stage primaryStage) {
        GameTimeOutViewController gameTimeOutViewController = (GameTimeOutViewController) fxmlLoader.getController();
        gameTimeOutViewController.getMainMenuPressed().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> source, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    showMainMenu();
                }
            }
        });
        gameTimeOutViewController.getExitPressed().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> source, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    System.exit(0);
                }
            }
        });
        return gameTimeOutViewController;
    }
    
    private FXMLLoader getFXMLLoader(String fxmlPath) {
        FXMLLoader fxmlLoader = new FXMLLoader();
        URL url = getClass().getResource(fxmlPath);
        fxmlLoader.setLocation(url);
        return fxmlLoader;
    }

    public static void main(String[] args) {
      
        launch(args);
    }
    
    protected static File showSaveGameDialog() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save Game");
        fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("XML", "*.xml"),
            new FileChooser.ExtensionFilter("All Files", "*.*")
        );
        if(MainApp.savedGameFile != null) {
            fileChooser.setInitialFileName(savedGameFile.getName());
            fileChooser.setInitialDirectory(MainApp.savedGameFile.getParentFile());
        }
        return fileChooser.showSaveDialog(MainApp.primaryStage);
    }
    
    private void showWaitingView(){
        FXMLLoader fxmlLoader = getFXMLLoader(WAITING_FXML_PATH);
        Parent root = null;
        try {
            root = getRoot(fxmlLoader);
        } catch (IOException ex) {
            Logger.getLogger(MainApp.class.getName()).log(Level.SEVERE, null, ex);
        }
        waitController = getWaitingController(fxmlLoader, primaryStage);

        Scene scene = new Scene(root, 600, 400);

        primaryStage.setTitle("Waiting for human players");
        primaryStage.setScene(scene);
        primaryStage.show();
        waitController.updateWithPlayers(clientGame.getGameName());
    }
    
    private void showJoinView(){
        FXMLLoader fxmlLoader = getFXMLLoader(JOIN_GAME_FXML_PATH);
        Parent playersRoot = null;
        try {
            playersRoot = getRoot(fxmlLoader);
        } catch (IOException ex) {
            Logger.getLogger(MainApp.class.getName()).log(Level.SEVERE, null, ex);
        }
        JoinGameViewController controller = getJoinGameController(fxmlLoader, primaryStage);

        Scene scene = new Scene(playersRoot, 600, 400);

        primaryStage.setTitle("Joins game");
        primaryStage.setScene(scene);
        primaryStage.show();
        controller.updateWithGames();
    }
    
    private WaitingViewController getWaitingController(FXMLLoader fxmlLoader, final Stage primaryStage) {
        WaitingViewController waitCon = (WaitingViewController) fxmlLoader.getController();

        waitCon.getBackButtonPressed().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> source, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    showMainMenu();
                }
            }
        });
        waitCon.getFinishedInit().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> source, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    try {
                        showGameView();
                    } catch (IOException ex) {
                        Logger.getLogger(MainApp.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InvalidParameters_Exception ex) {
                        Logger.getLogger(MainApp.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (GameDoesNotExists_Exception ex) {
                        Logger.getLogger(MainApp.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (PlayersManager.PlayerManagerException ex) {
                        Logger.getLogger(MainApp.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (Player.PlayerException ex) {
                        Logger.getLogger(MainApp.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
                         
       return waitCon;
    }
    
    private JoinGameViewController getJoinGameController(FXMLLoader fxmlLoader, final Stage primaryStage) {
        JoinGameViewController joinCon = (JoinGameViewController) fxmlLoader.getController();

        joinCon.getBackButtonPressed().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> source, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    showMainMenu();
                }
            }
        });
        joinCon.getFinishedInit().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> source, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    playerID = joinCon.getPlayerID();
                    MainApp.clientGame.setGameName(joinCon.getGameName());
                    MainApp.clientGame.setPlayerId(playerID);
                    MainApp.clientGame.setPlayerName(joinCon.getPlayerName());                    
                    showWaitingView();
                }
            }
        });
                         
       return joinCon;
    }
    
    public void setPlayerID(int id){
        this.playerID = id;
    }
}
