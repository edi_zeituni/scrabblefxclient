/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import game.model.ClientGame.GameException;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * FXML Controller class
 *
 * @author danielo
 */
public class ExitViewController implements Initializable {

    private SimpleBooleanProperty isCancelPressed = new SimpleBooleanProperty(false);
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }   
    
    @FXML
    protected void onSave(ActionEvent event) {
        if(MainApp.savedGameFile == null) {
            File file = MainApp.showSaveGameDialog();
            MainApp.savedGameFile = file;
        }
        //TODO save game
//        try {
//            MainApp.game.saveGame(MainApp.savedGameFile);
//        } catch(GameException ex) {
//             System.err.println(ex.text);
//        }
        System.exit(1);
    }
    
    @FXML
    protected void onDontSave(ActionEvent event) {
        System.exit(1);
    }
    
    @FXML
    protected void onCancel(ActionEvent event) {
        isCancelPressed.set(true);
        isCancelPressed.set(false);
    }
    
    public SimpleBooleanProperty getBackButtonPressed() {
        return isCancelPressed;
    }
}
