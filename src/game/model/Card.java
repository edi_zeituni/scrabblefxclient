/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.model;
/**
 *
 * @author Edi
 */
public class Card {
    private enum CardVal { 
        A(1), B(3), C(3), D(2), E(1), F(4), G(2), H(4), I(1), J(8), K(5), L(1), M(3), 
        N(1), O(1), P(3), Q(10), R(1), S(1), T(1), U(1), V(4), W(4), X(8), Y(4), Z(10),
        Joker(0);
        
        int val;
        
        CardVal(int val) {
            this.val = val;
        }
    }
    private char letter;
    private int value;
    
    public Card(char letter, int value)
    {
        cardInit(letter, value);
    }
    
    public Card(char letter) 
    {
        switch (letter) {
            case 'A':
                cardInit(letter, CardVal.A.val);
                break;
            case 'B':
                cardInit(letter, CardVal.B.val);
                break;
            case 'C':
                cardInit(letter, CardVal.C.val);
                break;                
            case 'D':
                cardInit(letter, CardVal.D.val);
                break;
            case 'E':
                cardInit(letter, CardVal.E.val);
                break;
            case 'F':
                cardInit(letter, CardVal.F.val);
                break;
            case 'G':
                cardInit(letter, CardVal.G.val);
                break;
            case 'H':
                cardInit(letter, CardVal.H.val);
                break;
            case 'I':
                cardInit(letter, CardVal.I.val);
                break;
            case 'J':
                cardInit(letter, CardVal.J.val);
                break;
            case 'K':
                cardInit(letter, CardVal.K.val);
                break;
            case 'L':
                cardInit(letter, CardVal.L.val);
                break;
            case 'M':
                cardInit(letter, CardVal.M.val);
                break;
            case 'N':
                cardInit(letter, CardVal.N.val);
                break;
            case 'O':
                cardInit(letter, CardVal.O.val);
                break;               
            case 'P':
                cardInit(letter, CardVal.P.val);
                break;
            case 'Q':
                cardInit(letter, CardVal.Q.val);
                break;
            case 'R':
                cardInit(letter, CardVal.R.val);
                break;
            case 'S':
                cardInit(letter, CardVal.S.val);
                break;
            case 'T':
                cardInit(letter, CardVal.T.val);
                break;
            case 'U':
                cardInit(letter, CardVal.U.val);
                break;
            case 'V':
                cardInit(letter, CardVal.V.val);
                break;
            case 'W':
                cardInit(letter, CardVal.W.val);
                break;
            case 'X':
                cardInit(letter, CardVal.X.val);
                break;
            case 'Y':
                cardInit(letter, CardVal.Y.val);
                break;
            case 'Z':
                cardInit(letter, CardVal.Z.val);
                break;
            case ' ': 
                cardInit(letter, CardVal.Joker.val);
                break;
        }
    }
     
    private void cardInit(char letter, int value)
    {
        this.letter = letter;
        this.value = value;
    }

    public char getLetter()
    {
        return this.letter;
    }
    public int getValue()
    {
        return this.value;
    }
    
    public void setJokerLetter(char ch)
    {
        if (this.letter == ' ') {
            this.letter = ch;
        }
    }
}