/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.model;

/**
 *
 * @author Edi
 */
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Players {
    private Set<Player> players;

    public Players() {
	players = new HashSet<>();
    }

    public boolean isPlayerExists(Player player){
	return players.contains(player);
    }

    public void addPlayer (Player player){
	players.add(player);
    }

//    public void setPlayers (Collection<Player> players) {
//	players.clear();
//	players.addAll(players);
//    }

    public Collection<Player> getPlayers(){
	return Collections.unmodifiableSet(players);
    }
    
    public int size(){
        return players.size();
    }
    
    public void remove(Player p) {
        players.remove(p);
    }
    
    public void removeAll(){
        System.err.println("ALL PLAYERS REMOVED");
        players.clear();
    }
}

