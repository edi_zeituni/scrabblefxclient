/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.model;

import game.GameViewController;
import game.MainApp;
import game.WaitingViewController;
import game.model.Player.PlayerException;
import game.ws.Event;
import game.ws.EventType;
import game.ws.GameDoesNotExists_Exception;
import game.ws.InvalidParameters_Exception;
import game.ws.Orientation;
import game.ws.PlayerDetails;
import game.ws.PlayerType;
import java.io.File;
import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author Edi
 */
public class ClientGame {
    private String gameName;
    private Board board;
    private PlayersManager playerManager;
    private List<Event> eventList;
    private GameViewController controller;
    private String winnerPlayerName;
    private int clientPlayerID;
    private int lastEventID;
    private String clientPlayerName;
    private Thread runLoop;
    
    private final int MAX_PLAYERS = 4;
    private final Boolean HUMAN = true;
    private final Boolean COMPUTER = false;
    
    public ClientGame(){
        playerManager = new PlayersManager();
        eventList = null;
    }
    
    public void initGameByPullEvents() throws InvalidParameters_Exception, GameDoesNotExists_Exception, PlayersManager.PlayerManagerException, PlayerException{
        playerManager.removeAllPlayers();
        board = new Board();
        eventList = MainApp.gameWebService.getEvents(clientPlayerID, 0);
        int i = 0;
        for (Event event : eventList) {
            if (event.getType() == EventType.LETTERS_ON_BOARD) {
                board.setCell(event.getPosition(),event.getLetters().charAt(0));
                i++;
            }
            else
                break;
        }
        
        for (int j = 0; j < i; j++) {
            eventList.remove(0);
        }

        if (eventList.isEmpty() || eventList.get(0).getType() != EventType.GAME_START) {
            throw new InvalidParameters_Exception("Failed to initializing game with event list", null);
        }
        lastEventID = eventList.get(0).getId();
        List<PlayerDetails> playersInfo = MainApp.gameWebService.getPlayersDetails(gameName);
        Player newPlayer;
        
        for (PlayerDetails player: playersInfo) {
            newPlayer = playerManager.addPlayer(player.getName(), player.getType() == PlayerType.HUMAN);
            newPlayer.setScore(player.getScore());
            
            if (player.getName().equals(clientPlayerName)) {
                newPlayer.setLetters(player.getLetters());
                playerManager.setCurrentPlayer(newPlayer);
                playerManager.setClientPlayer(newPlayer);
            }
        }
    }
    
    public void startGame(GameViewController controller){
        this.controller = controller;
        
        runLoop = new Thread(()->startRunLoop());
        runLoop.start();
    }
    
    public void stopGame(){
        if (runLoop != null) {
            runLoop.stop();
        }
    }
    
    public void startRunLoop(){
        while (true){
            if (eventList.isEmpty()) {
                try {
                    eventList = MainApp.gameWebService.getEvents(clientPlayerID, lastEventID);
                } catch (InvalidParameters_Exception ex) {
                    Logger.getLogger(ClientGame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (eventList.isEmpty()) {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ClientGame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else{
                Event event = eventList.remove(0);
                processEvent(event);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ClientGame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    private void processEvent(Event event){
        lastEventID = event.getId();
        switch(event.getType())
        {
            case GAME_OVER:
                Platform.runLater(() -> {controller.notifyGameIsOver();});
                break;
            case GAME_START:
                //we are inside the run loop, it means that the game has started and there is no need to take any action
                //for this event
                break;
            case GAME_WINNER:
                winnerPlayerName = event.getPlayerName();
                break;
            case LETTERS_ON_BOARD:
                processLettersOnBoard(event);
                break;
            case PLAYER_ACTION:
                processPlayerAction(event);
                break;
            case PLAYER_RESIGNED:
                processResigned(event);
                break;
            case PLAYER_TURN:
                processPlayerTurn(event);
                break;
            case PROMPT_PLAYER_TO_TAKE_ACTION:
                processPromptPlayerToTakeAction(event);
                break;
            default:
                break;
        }
    }
    
    private void processResigned(Event event) {
        String name = event.getPlayerName();
        
        if(clientPlayerName.equals(event.getPlayerName())) {
            Platform.runLater(() -> {controller.notifyGameTimout();});
        } 
    }
    
    private void processPromptPlayerToTakeAction(Event event){
        if (event.getPlayerName().equals(clientPlayerName))
            controller.getActionFromPlayer(event.getTimeout());
    }
    
    private void processPlayerTurn(Event event){
        playerManager.setCurrentPlayer(event.getPlayerName());
        controller.changeCurrentPlayer();
    }
    
    private void processLettersOnBoard(Event event){
        //first we change the client board and then the board view component
        board.changeCells(event.getPosition(), event.getOrientation(), event.getLetters());
        controller.updateBoardWithNewLetters(event.getPosition(), event.getOrientation(), event.getLetters());
    }
    
    private void processPlayerAction(Event event){
        switch(event.getPlayerAction()){
            case MAKE_WORD:
                processMakeWord(event);
                break;
            case PASS_TURN:
                controller.notifyPassAction();
                break;
            case REPLACE_LETTERS:
                processReplaceLetters(event);
                break;
            default:
                break;
        }
    }
    
    private void processMakeWord(Event event){
        Player p = playerManager.getPlayerByName(event.getPlayerName());
        p.setScore(p.getScore() + event.getScore());
        
        if (event.getScore()>0 && event.getPlayerName().equals(clientPlayerName))
        {
            try {
                PlayerDetails playerInfo = MainApp.gameWebService.getPlayerDetails(clientPlayerID);
                controller.updateClientPlayerCards(playerInfo.getLetters());
            } catch (GameDoesNotExists_Exception ex) {
                Logger.getLogger(ClientGame.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvalidParameters_Exception ex) {
                Logger.getLogger(ClientGame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private void processReplaceLetters(Event event){
        try {
            if (!event.getPlayerName().equals(clientPlayerName))
                return;
            PlayerDetails playerInfo = MainApp.gameWebService.getPlayerDetails(clientPlayerID);
            controller.updateClientPlayerCards(playerInfo.getLetters());
        } catch (GameDoesNotExists_Exception ex) {
            Logger.getLogger(ClientGame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidParameters_Exception ex) {
            Logger.getLogger(ClientGame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void passTurn() throws InvalidParameters_Exception{
        MainApp.gameWebService.passTurn(clientPlayerID, lastEventID);
    }
    
    public void MakeWord(String lettersStartPosition, Orientation lettersOrientation, 
                         String letters, String wordStartPosition, Orientation wordOrientation, String word) throws InvalidParameters_Exception{
        MainApp.gameWebService.makeWord(clientPlayerID, lastEventID, lettersStartPosition, 
                                        lettersOrientation, letters, wordStartPosition, wordOrientation, word);
    }
    
    public void replaceCards(String letters) throws InvalidParameters_Exception{
        MainApp.gameWebService.replaceLetters(clientPlayerID, lastEventID, letters);
    }
    
    public void setPlayerId(int playerID){
        this.clientPlayerID = playerID;
    }
    
    public void setPlayerName(String name){
        clientPlayerName = name;
    }
    
    public void resetGame() throws PlayerException{  
        board = new Board();
        playerManager.resetPlayers();
    }
    
    public void createNewGame() throws Deck.DeckException, GameException {
        board = new Board();
    }  
    
    public PlayersManager getPlayersManager(){
        return this.playerManager;
    }
    
    public String getWinnerPlayerName(){
        return winnerPlayerName;
    }
    public Board getBoard(){
        return this.board;
    }
    
    public static class GameException extends Exception {
        public enum ErrorType {LOAD_GAME_ERROR, SAVE_GAME_ERROR, PASS_TURN_ERROR, WORD_VALIDATION_ERROR, 
                                INTERNET_CONNECTION_ERROR, NO_HUMAN_PLAYER};
        public String text;
        public String type;
        public int code;
        
        public GameException(ErrorType type) {
            this.type = type.toString();
            switch (type){
                case LOAD_GAME_ERROR:
                    code = 101;
                    text = "Error Loading Game From File";
                    break;
                case SAVE_GAME_ERROR:
                    code = 102;
                    text = "Error Saving Game To File";
                    break;
                case PASS_TURN_ERROR:
                    code = 103;
                    text = "Passing Turn Encountered An Error";
                    break;
                case WORD_VALIDATION_ERROR:
                    code = 104;
                    text = "Word Validation Error";
                    break; 
                case INTERNET_CONNECTION_ERROR:
                    code = 105;
                    text = "Check your internet connection";
                    break;
                case NO_HUMAN_PLAYER:
                    code = 106;
                    text = "Human player required";
                    break;
                    
            }
        }
    }
    
    public void setGameName(String name){
        this.gameName = name;
    }
    
    public String getGameName(){
        return this.gameName;
    }
    
}
