/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.model;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author danielo
 */
public class Player {
    public enum PlayerType { Human, Computer };
    private int score;
    private String letters;
    
    public static final int CARDS_FOR_PLAYER = 7;
    private final String NAME;
    private final PlayerType TYPE;

    
    public Player(String name, Boolean isHuman){
        this.NAME = name;
        this.TYPE = isHuman ? PlayerType.Human : PlayerType.Computer;
        score = 0;
        letters = null;
    }
    
    public String getName() {
        return this.NAME;
    }
    
    public int getScore() {
        return this.score;
    }
    
    public PlayerType getType() {
        return this.TYPE;
    }
    
    public void reset() {
        letters = "";
        score = 0;
    }
    
    public String getCardsLetters(){
        return letters;
    }
    
    public void updateScore(int points)
    {
        this.score += points;
    }
    
    public void setLetters(String letters){
        this.letters = letters;
    }
    
    public void setScore(int score) {
        this.score = score;    
    }
    
    @Override
    public int hashCode() {
	int hash = 7;
	hash = 47 * hash + Objects.hashCode(this.NAME);
	return hash;
    }

    @Override
    public boolean equals(Object obj) {
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	final Player other = (Player) obj;
	if (!Objects.equals(this.NAME, other.NAME)) {
	    return false;
	}
	return true;
    }
    
    public boolean isHuman() {
        return this.TYPE == PlayerType.Human;
    }
    
    public static class PlayerException extends Exception {
        public enum ErrorType { PLAYER_FULL_OF_CARDS,
                                PLAYER_HAS_NO_CARDS,
                                PLAYER_DOESNT_HOLD_CARD};
        public String text;
        public String type;
        public int code;

        public PlayerException(ErrorType type, String additionalData) {
            this.type = type.toString();
            switch (type){
                case PLAYER_FULL_OF_CARDS:
                    code = 501;
                    text = "Player already have cards";
                    break;
                case PLAYER_HAS_NO_CARDS:
                    code = 502;
                    text = "Player doesn't hold cards";
                    break;
                case PLAYER_DOESNT_HOLD_CARD:
                    code = 503;
                    text = "Player doesn't hold card: " + additionalData;
                    break;
                default:
                    code = 500;
                    text = "General error: " + additionalData;
                    break;
            }
        }
    }

}
