/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;


/**
 *
 * @author Edi
 */
public class Deck {
    public static final int DECK_SIZE = 100;
    ArrayList<Card> cardList;
    
    public Deck(){
        cardList = new ArrayList<Card>();
        initDeck();
    }
    
    public Boolean isEmpty(){
        return cardList.size() == 0;
    }
    
    public Card deal() throws DeckException {
        if (this.isEmpty()) // check for an empty deck
            throw new DeckException(DeckException.ErrorType.DECK_IS_EMPTY,null);

        Card res = cardList.remove(cardList.size()-1);
        return res;
    }
    
    public Card exchange(Card oldCard) throws DeckException{
        Card newCard = oldCard;

        if (cardList.size() < 7) {
            throw new DeckException(DeckException.ErrorType.DECK_IS_EMPTY,null);
        }
        else
        {
            Random randIndex = new Random();
            // pick a random index between 0 and cardsInDeck - 1
            int newI = randIndex.nextInt(cardList.size());
            // swap oldCard and cards[newI]
            newCard = cardList.get(newI);
            cardList.set(newI,oldCard);
        }
        return newCard;
    }
    
    public void shuffle() {
        Collections.shuffle(cardList);
    }
       
    private void createCards(char letter, int amount, int index){
        Card card;
        for (int i = 0; i < amount; i++ , index++) {
            card = new Card(letter);
            cardList.add(card);
        }
    }
    
    private void initDeck(){
        int index = 0;
        createCards('A',9,index);
        createCards('B',2,index+=9);
        createCards('C',2,index+=2);
        createCards('D',4,index+=2);
        createCards('E',12,index+=4);
        createCards('F',2,index+=12);
        createCards('G',3,index+=2);
        createCards('H',2,index+=3);
        createCards('I',9,index+=2);
        createCards('J',1,index+=9);
        createCards('K',1,index+=1);
        createCards('L',4,index+=1);
        createCards('M',2,index+=4);
        createCards('N',6,index+=2);
        createCards('O',8,index+=6);
        createCards('P',2,index+=8);
        createCards('Q',1,index+=2);
        createCards('R',6,index+=1);
        createCards('S',4,index+=6);
        createCards('T',6,index+=4);
        createCards('U',4,index+=6);
        createCards('V',2,index+=4);
        createCards('W',2,index+=2);
        createCards('X',1,index+=2);
        createCards('Y',2,index+=1);
        createCards('Z',1,index+=2);
        createCards(' ',2,index+=1);
    }
    
    public void remvoCard(Card card) throws DeckException{
        char letter = card.getLetter();
        Card tmp;
        for(int i=0; i < cardList.size() ; i++) {
            tmp = cardList.get(i);
            if(tmp.getLetter() == letter){
                cardList.remove(i);
                return;
            }
        }
        
        throw new DeckException(DeckException.ErrorType.CARD_NOT_FOUND, String.valueOf(letter));
    }
    
    public int numeOfCardInDeck() {
        return cardList.size();
    }

    public static class DeckException extends Exception {
        public enum ErrorType { DECK_IS_EMPTY,
                                SHUFFLE_USED_DECK,
                                CARD_NOT_FOUND
                                };
        public String text;
        public String type;
        public int code;
        
        public DeckException(ErrorType type, String additionalData) {
            this.type = type.toString();
            switch (type){
                case DECK_IS_EMPTY:
                    code = 101;
                    text = "Not have enough cards in deck";
                    break;
                case SHUFFLE_USED_DECK:
                    code = 102;
                    text = "Cannot shuffle an used deck.";
                    break;
                case CARD_NOT_FOUND:
                    code = 103;
                    text = "Card " + additionalData + " not found in deck.";
                    break;
                default:
                    code = 100;
                    text = "Deck General error: " + additionalData;
                    break;
            }
        }
    }
    
        public int size(){
            return this.cardList.size();
        }
}
