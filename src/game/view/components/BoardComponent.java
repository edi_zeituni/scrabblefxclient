/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.view.components;

import game.model.Board;
import game.model.Cell;
import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import game.ws.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.GridPane;

/**
 *
 * @author Edi
 */
public class BoardComponent extends GridPane {
    private ArrayList<CellComponent> changedCellsList;
    private CellComponent jokerToBeReplacedWithCard;
    SimpleStringProperty jokerDroppedOnCell;
    private String lettersStartPosForLater;
    private Orientation lettersOrientationForLater;
    private String lettersForLater;
    
    public BoardComponent(Board board) throws Cell.CellException{
        changedCellsList = new ArrayList<>();
        setAlignment(Pos.CENTER);
        jokerDroppedOnCell = new SimpleStringProperty("");
        
        CellComponent newCell;
        for (int i = 0; i < Board.COL; i++) {
            for (int j = 0; j < Board.ROW; j++) {
                Cell modelCell = board.getCell(i, j);
                if (i == 7 && j==7)
                    newCell = new CellComponent("center-cell",i,j);
                else
                    newCell = new CellComponent(imageNameByBonusType(modelCell.getCellBonus()),i,j);
                if (!modelCell.isEmpty()) 
                    newCell.setCard(String.valueOf(modelCell.getCard().getLetter()));
                newCell.setChangedList(changedCellsList);
                newCell.getCellListenToJoker().addListener(new ChangeListener<String>() { 
                    @Override
                    public void changed(ObservableValue<? extends String> observableValue, String oldValue, String newValue) {
                       jokerDroppedOnCell.setValue(newValue);
                    }
                 });
                add(newCell, i, j);
            }
        }
        setGridLinesVisible(true);
    }
    
    private String imageNameByBonusType(Cell.CellBonus bonusType)
    {
        String res = "cardBackground";
        switch(bonusType){
            case DoubleLetter:
                res = "dl-cell";
                break;
            case TripleLetter:
                res = "tl-cell";
                break;
            case DoubleWord:
                res = "dw-cell";
                break;
            case TripleWord:
                res = "tw-cell";
                break;
            default:
                res = "regular-cell";
                break;
        }
        return res;
    }
    
    public ArrayList<CellComponent> getListOfChangedCells(){
        return this.changedCellsList;
    }
    
    public SimpleStringProperty getCellListenToJoker(){
        return jokerDroppedOnCell;
    }
    
    public CellComponent getCellByPos(int column, int row){

        CellComponent result = null;
        ObservableList<Node> childrens = getChildren();
        for(Node node : childrens) {
            if(getRowIndex(node) == row && getColumnIndex(node) == column) {
                result = (CellComponent)node;
                break;
            }
        }
        return result;
    }
    
    public void commitCardsToBoard(){
        for (CellComponent cell : changedCellsList) {
            cell.setDragable(false);
        }
        changedCellsList.clear();
    }

    public void removeUncomittedCardsFromBoard(){
        for (CellComponent cell : changedCellsList) {
            cell.removeCard();
        }
        changedCellsList.clear();
    }
    
    public Orientation getOrientationForSingleCardOnBoard(){
        if(changedCellsList.isEmpty()) 
            return Orientation.HORIZONTAL;
        
        CellComponent previousCell;
        CellComponent cell = changedCellsList.get(0);
        int col = cell.getXPos();
        int row = cell.getYPos();
        
        if(col-1 >= 0){
            if(!getCellByPos(col-1, row).isEmpty)
                return Orientation.HORIZONTAL;
        }
        if (row-1 >= 0) {
            if(!getCellByPos(col, row-1).isEmpty)
                return Orientation.VERTICAL;
        }
        if(col+1 <= 14) {
            if(!getCellByPos(col+1, row).isEmpty)
                return Orientation.HORIZONTAL;
        }
        //default orientation 
        return Orientation.VERTICAL;
    }
    
    public void setValuesForLaterChangeCells(String lettersStartPos, 
                                          Orientation lettersOrientation,
                                          String letters)
    {
        lettersForLater = letters;
        lettersOrientationForLater = lettersOrientation;
        lettersStartPosForLater = lettersStartPos;
    }
    
    private  int getColNumFromFormat(String position){
        return position.charAt(0) - 'A';
    }
    
    private int getRowNumFromFormat(String position){
        int row = position.charAt(1) - '1';
        if (position.length() == 3 ){
            row = position.charAt(1) - '0';
            row *= 10;
            row += position.charAt(2) - '1';
        }
        return row;
    }
    
    public void changeLettersOnBoardWithLaterValues(){
        removeUncomittedCardsFromBoard();
        CellComponent emptyCellComp = null;
        int row = getRowNumFromFormat(lettersStartPosForLater);
        int col = getColNumFromFormat(lettersStartPosForLater);
        int addToRow = 0;
        int addToCol = 0;
        if (lettersOrientationForLater == Orientation.HORIZONTAL) 
            addToCol = 1;
        else
            addToRow = 1;
        
        for (int i = 0; i < lettersForLater.length(); i++) {
            emptyCellComp = getCellByPos(col, row);
            changedCellsList.add(emptyCellComp);
            emptyCellComp.setCard(String.valueOf(lettersForLater.charAt(i)));
            col +=addToCol;
            row +=addToRow;
        }
        lettersStartPosForLater = null;
        lettersForLater = null;
        lettersOrientationForLater = null;
    }
}
