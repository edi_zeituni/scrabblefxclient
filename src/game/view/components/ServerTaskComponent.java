/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.view.components;

import game.ws.ScrabbleWebService;
import game.ws.ScrabbleWebServiceService;
import java.net.URL;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


/**
 *
 * @author danielo
 */
public class ServerTaskComponent {
    ExecutorService executor;
    ScrabbleWebService webService;
    Future future;
    
    public ServerTaskComponent() {
        executor = Executors.newSingleThreadExecutor();
    }
    
    public void cancelServerTask() {
        if(future != null && future.isDone() == false) {
            future.cancel(true);
        }
    }
    
    public void connectServerTask(URL location) throws ExecutionException, InterruptedException, TimeoutException {
        if(future != null && future.isDone() == false) {
            throw new ExecutionException("Task is already in progress...", null);
        }
        
        future = executor.submit(new ServerTaskComponent.ConnectServerClass(location));
        future.get(3, TimeUnit.SECONDS);
    }
    
    private class ConnectServerClass implements Runnable {
        URL location;
        
        public ConnectServerClass(URL location) {
            this.location = location;
        }
        
        @Override
        public void run() {
            ScrabbleWebServiceService service = new ScrabbleWebServiceService(location);
            webService = service.getScrabbleWebServicePort();
        }
    }
}
