/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.view.components;

import javafx.beans.property.SimpleStringProperty;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;

/**
 *
 * @author Edi
 */
public class ReplaceJokerComponent extends FlowPane{
    @FXML
    GridPane cardsPane;
    
    @FXML
    Label title;
    
    @FXML
    Button choose;
    
    @FXML
    Button cancel;
    SimpleStringProperty replacedCardLetter;
    
    public ReplaceJokerComponent(double width, double height) {
        setPrefSize(390, height);
        setAlignment(Pos.CENTER);
        setHgap(100);
        setVgap(100);
        replacedCardLetter = new SimpleStringProperty(" ");
        title = new Label("Choose a card for your joker");
        cardsPane = new GridPane();
        
        CellComponent newCell;
        char letter = 'A';
        for (int i = 0; i < 13; i++) {
            for (int j = 0; j < 2; j++) {
                newCell = new CellComponent(String.valueOf(letter), i, j, String.valueOf(letter));
                newCell.setDragable(false);
                newCell.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        replacedCardLetter.setValue(((CellComponent)event.getTarget()).getCardName());
                    }
                });
                cardsPane.add(newCell, i, j);
                letter++;
            }
        }
        cardsPane.setGridLinesVisible(true);
        
        getChildren().add(title);
        getChildren().add(cardsPane);
    }
    
    public SimpleStringProperty getReplacedCardProperty(){
        return replacedCardLetter;
    }
    
}
