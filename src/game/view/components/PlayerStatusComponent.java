/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.view.components;

import game.view.utils.ImageUtils;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.ImageViewBuilder;
import javafx.scene.layout.HBox;

/**
 *
 * @author Edi
 */
public class PlayerStatusComponent extends HBox {
    @FXML
    private Label score;
    private String name;
    
    public PlayerStatusComponent(String title, boolean isHuman, int score) {
        name = title;
        setAlignment(Pos.TOP_LEFT);
        setSpacing(20);
        setPrefWidth(145);
        setPrefHeight(45);
        this.score = createLabel(String.valueOf(score));
        getChildren().addAll(createImage(isHuman), createLabel(title) , this.score);
    }
    
    private Label createLabel(String title){
        return new Label(title);
    }
    
    private ImageView createImage(boolean isHuman){
        ImageView img = ImageViewBuilder
                .create()
                .image(getImage(isHuman))
                .build();
        img.setFitHeight(40);
        img.setFitHeight(40);
        img.setPreserveRatio(true);
        img.setSmooth(true);
        
        return img;
    }

    private Image getImage(boolean isHuman) {
        String filename = isHuman ? "human" : "computer";
        return ImageUtils.getImage(filename);
    }
    
    public String getName() {
        return name;
    }
    
    public void setScore(int score) {
        this.score.setText(String.valueOf(score));
    }
}
