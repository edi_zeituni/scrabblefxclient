/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.view.components;

import game.view.utils.ImageUtils;
import java.util.ArrayList;
import java.util.Comparator;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;

/**
 *
 * @author danielo
 */
public class CellComponent extends ImageView {
    Image originalImage;
    String cardName;
    Boolean isEmpty;
    Boolean isDragable;
    int x;
    int y;
    private ArrayList<CellComponent> changedCellsOnBoardList;
    SimpleStringProperty jokerDroppedOnCell;
    
    public CellComponent(String cellName, int x, int y, String cardName)
    {
        this(cellName, x, y);
        this.cardName = cardName;
        setCard(cardName);
        isDragable = false;
    }
    
    public CellComponent(String cellName, int x, int y) {
        jokerDroppedOnCell = new SimpleStringProperty("");
        cardName = "";
        isEmpty = true;
        isDragable = false;
        this.x = x;
        this.y = y;
        setFitHeight(30);
        setFitWidth(30);
        setPreserveRatio(true);
        setSmooth(true);
        originalImage = getImageFromIconResource(cellName);
        setImage(originalImage);   
        
        setOnDragOver(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                /* data is dragged over the target */
                System.out.println("onDragOver");
                
                /* accept it only if it is  not dragged from the same node 
                 * and if it has a string data */
                if (event.getGestureSource() != this &&
                        event.getDragboard().hasImage() && isEmpty) {
                    /* allow for both copying and moving, whatever user chooses */
                    event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                }
                
                event.consume();
            }
        });
        
        setOnDragDropped(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                /* data dropped */
                System.out.println("onDragDropped");
                /* if there is a string data on dragboard, read it and use it */
                Dragboard db = event.getDragboard();
                boolean success = false;
                if (db.hasImage() && isEmpty) {
                    setImage(db.getImage());
                    cardName = db.hasString() ? db.getString() : cardName;
                    if (cardName.equals("joker")) {
                        jokerDroppedOnCell.setValue(x + "," + y);
                    }
                    isEmpty = false;
                    isDragable = true;
                    success = true;
                    if (changedCellsOnBoardList!=null)
                        addToChangedList();
                }
                /* let the source know whether the string was successfully 
                 * transferred and used */
                event.setDropCompleted(success);
                
                event.consume();
            }
        });
        
        setOnDragDetected(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (!isDragable) {
                    return;
                }
               /* drag was detected, start drag-and-drop gesture*/
                System.out.println("onDragDetected");

                /* allow any transfer mode */
                Dragboard db = startDragAndDrop(TransferMode.ANY);

                /* put a string on dragboard */
                ClipboardContent content = new ClipboardContent();
                content.putImage(getImage());
                content.putString(cardName);
                setImage(originalImage);
                db.setContent(content);

                event.consume();
            }
        });
        
        setOnDragDone(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                /* the drag-and-drop gesture ended */
                System.out.println("onDragDone");
                /* if the data was successfully moved, clear it */
                if (event.getTransferMode() == TransferMode.MOVE) {
                    isEmpty = true;
                    isDragable = false;
                    cardName = "";
                    setImage(originalImage);
                    if (changedCellsOnBoardList!=null)
                        removeFromChangedList();
                }
                else{
                    Dragboard db = event.getDragboard();
                    if (db.hasImage()) {
                        setImage(db.getImage());
                        isEmpty = false;
                    }
                }
                
                event.consume();
            }
        });
        
    }
    
    private Image getImageFromIconResource(String cardName) {
        return ImageUtils.getImage(cardName);
    }
    
    public void setCard(String cardName){
        this.isEmpty = false;
        if (cardName.equals(" ")) 
            cardName = "joker";
        this.cardName = cardName;
        setImage(getImageFromIconResource(cardName));
    }
    
    public void setImageOnEmptyCell(Image img){
        this.isEmpty = false;
        setImage(img);
    }
    
    public void removeCard(){
        setImage(originalImage);
        this.isEmpty = true;
        jokerDroppedOnCell.setValue("");
    }
    
    public void setDragable(Boolean isDragable){
        this.isDragable = isDragable;
    }
    
    public void setSize(int height, int width){
        setFitHeight(height);
        setFitWidth(width);
    }
    
    public void setChangedList(ArrayList<CellComponent> list){
        changedCellsOnBoardList = list;
    }
    
    public SimpleStringProperty getCellListenToJoker(){
        return jokerDroppedOnCell;
    }
    
    private void removeFromChangedList(){
        changedCellsOnBoardList.remove(this);
    }
    private void addToChangedList(){
        changedCellsOnBoardList.add(this);
    }
    
    public Boolean isEmpty(){
            return isEmpty;
    }
    
    public String getCardName(){
        return cardName;
    }
    
    public int getXPos() {
        return x;
    }
    
    public int getYPos() {
        return y;
    }
    
    public static Comparator<CellComponent> CellVerticalComparator = (CellComponent cell1, CellComponent cell2) -> cell1.getYPos() - cell2.getYPos();
    
    public static Comparator<CellComponent> CellHorizontalComparator = (CellComponent cell1, CellComponent cell2) -> cell1.getXPos() - cell2.getXPos();
}
