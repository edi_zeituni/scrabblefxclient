/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.view.utils;

import javafx.scene.image.Image;

/**
 *
 * @author Edi
 */
public class ImageUtils {
    private static final String RESOURCES_DIR = "/";
    private static final String IMAGES_DIR = RESOURCES_DIR + "icons/";
    private static final String IMAGE_EXTENSION = ".png";
    
    public static Image getImage (String filename){
        if (filename == null || filename.isEmpty()) {
            return null;
        }
        
        if (!filename.endsWith(IMAGE_EXTENSION)){
            filename = filename + IMAGE_EXTENSION;
        }
        
        return new Image(ImageUtils.class.getResourceAsStream(IMAGES_DIR + filename));
    }
}
