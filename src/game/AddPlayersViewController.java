/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import game.ws.DuplicateGameName_Exception;
import game.ws.InvalidParameters_Exception;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Edi
 */
public class AddPlayersViewController implements Initializable {
    @FXML
    private TextField playerNameTextField, numOfHuman, numOfComputer, gameName;
    
    @FXML
    private Label errorMessageLabel;

    @FXML
    private Button continueButton, backButton;
    
    private boolean isErrorMessageShown = false;
    private SimpleBooleanProperty finishedInit;
    private SimpleBooleanProperty isBackPressed;
    int playersCounter = 0;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        playerNameTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                onPlayerNameChange();
            }
        });

        finishedInit = new SimpleBooleanProperty(false);
        isBackPressed = new SimpleBooleanProperty(false);
    }    

    @FXML
    protected void onPlayerNameChange() {
        updateContinueButtonState();
    }
    
    @FXML
    protected void onContinue (ActionEvent event){
        continueButton.setDisable(true);
        
        try {
            MainApp.gameWebService.createGame(gameName.getText(), Integer.valueOf(numOfHuman.getText()), Integer.valueOf(numOfComputer.getText()));
            finishedInit.set(true);
        } catch (DuplicateGameName_Exception ex) {
            errorMessageLabel.setText(ex.getMessage());
            errorMessageLabel.setVisible(true);
        } catch (InvalidParameters_Exception ex) {
            errorMessageLabel.setText(ex.getMessage());
            errorMessageLabel.setVisible(true);;
        }
    }
    
    @FXML
    protected void onBack(ActionEvent event) {
        isBackPressed.setValue(true);
        isBackPressed.setValue(false);
    }
    
    public SimpleBooleanProperty getBackButtonPressed() {
        return isBackPressed;
    }

    public SimpleBooleanProperty getFinishedInit() {
        return finishedInit;
    }

    private void updateContinueButtonState() {
        boolean isEmptyName = getPlayerName().trim().isEmpty();
        boolean disable = isEmptyName || isErrorMessageShown;
        continueButton.disableProperty().setValue(disable);
    }

    public String getPlayerName() {
        return playerNameTextField.getText();
    }
    
    public String getGameName() {
        return gameName.getText();
    }
    
    private void clearPlayerNameField() {
        playerNameTextField.clear();
        playerNameTextField.requestFocus();
    }
    
    public void setErrorMessage(String message){
        this.errorMessageLabel.setText(message);
        errorMessageLabel.setVisible(true);
    }
}
