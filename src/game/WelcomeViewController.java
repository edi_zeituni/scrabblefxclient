/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * FXML Controller class
 *
 * @author Edi
 */
public class WelcomeViewController implements Initializable {
    private SimpleBooleanProperty didNewGameSelected;
    private SimpleBooleanProperty didLoadGameSelected;
    private SimpleBooleanProperty didJoinGameSelected;
    private int playerID;
    private String gameName;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        didNewGameSelected = new SimpleBooleanProperty(false);
        didLoadGameSelected = new SimpleBooleanProperty(false);
        didJoinGameSelected = new SimpleBooleanProperty(false);
    }    
    
    @FXML
    protected void onNewGame (ActionEvent event){
        didNewGameSelected.set(true);
        didNewGameSelected.set(false);
    }
    
    @FXML
    protected void onLoadGame (ActionEvent event){
        didLoadGameSelected.set(true);
        didLoadGameSelected.set(false);
    }
    
    @FXML
    protected void onJoinGame (ActionEvent event){
        didJoinGameSelected.set(true);
        didJoinGameSelected.set(false);
    }
    
    @FXML
    protected void onExit (ActionEvent event){
        System.exit(1);
    }

    public SimpleBooleanProperty getNewGameSelected() {
        return didNewGameSelected;
    }
    
    public SimpleBooleanProperty getLoadGameSelected() {
        return didLoadGameSelected;
    }
    
    public SimpleBooleanProperty getJoinGameSelected() {
        return didJoinGameSelected;
    }
    
    public int getPlayerID(){
        return playerID;
    }
    
    public String getGameName(){
        return this.gameName;
    }
}
