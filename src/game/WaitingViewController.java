/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import game.model.Player;
import game.model.PlayersManager;
import game.view.components.PlayerStatusComponent;
import game.ws.GameDoesNotExists_Exception;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import game.ws.GameDetails;
import game.ws.GameStatus;
import game.ws.PlayerDetails;
import game.ws.PlayerType;
import java.util.List;
import javafx.application.Platform;

/**
 * FXML Controller class
 *
 * @author Edi
 */
public class WaitingViewController implements Initializable {
    
    private SimpleBooleanProperty isBackPressed, finishedInit;
    private String gameName;
    @FXML
    private Label statusLabel;
    @FXML
    private Button backButton;
    @FXML
    private Pane playersPane;
    
    private List<PlayerDetails> playersInfo = null;
    private int joined;
    private int numOfHumans;
    private int numOfComputers;
    private Thread poolThread;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        isBackPressed = new SimpleBooleanProperty(false);
        finishedInit = new SimpleBooleanProperty(false);
    }
    
    public void updateWithPlayers(String gameName){
        this.gameName = gameName;
        poolThread = new Thread(() -> {
            pullGameAndPlayersInfo();
        });
        poolThread.start();
    }
    
    public void pullGameAndPlayersInfo()
    {
        GameStatus status;
        try {
            GameDetails gameInfo = MainApp.gameWebService.getGameDetails(gameName);
            numOfHumans = gameInfo.getHumanPlayers();
            numOfComputers = gameInfo.getComputerizedPlayers();
            joined = gameInfo.getJoinedHumanPlayers();
            while (gameInfo.getStatus() == GameStatus.WAITING){
                playersInfo = MainApp.gameWebService.getPlayersDetails(gameName);
                Platform.runLater(()-> reloadPlayersPane());
                Thread.sleep(5000);
                gameInfo = MainApp.gameWebService.getGameDetails(gameName);
                numOfHumans = gameInfo.getHumanPlayers();
                numOfComputers = gameInfo.getComputerizedPlayers();
                joined = gameInfo.getJoinedHumanPlayers();
            }
            
            playersInfo = MainApp.gameWebService.getPlayersDetails(gameName);
            Platform.runLater(()-> reloadPlayersPane());
            Platform.runLater(() -> {
                finishedInit.set(true);
            });
            
        } catch (GameDoesNotExists_Exception ex) {
            Logger.getLogger(WaitingViewController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(WaitingViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void reloadPlayersPane(){
        playersPane.getChildren().removeAll(playersPane.getChildren());
        for (PlayerDetails player: playersInfo) {
            PlayerStatusComponent playerView = new PlayerStatusComponent(player.getName(), 
                                                    player.getType() == PlayerType.HUMAN,
                                                    player.getScore());
            playersPane.getChildren().add(playerView);
        }
        statusLabel.setText("joined " + String.valueOf(numOfComputers + joined) + "/" + String.valueOf(numOfComputers + numOfHumans));
    }
    
    public SimpleBooleanProperty getBackButtonPressed() {
        return isBackPressed;
    }
    
    @FXML
    protected void onBack(ActionEvent event) {
        if (poolThread != null) 
            poolThread.stop();
        isBackPressed.setValue(true);
        isBackPressed.setValue(false);
    }
    
    public SimpleBooleanProperty getFinishedInit() {
        return finishedInit;
    }
    
}
