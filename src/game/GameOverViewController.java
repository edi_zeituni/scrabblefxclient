/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import game.model.PlayersManager;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.text.Text;




public class GameOverViewController implements Initializable {
    
    @FXML
    Text winnerNameLabel;
    
    @FXML
    Button mainMenuButton;
    
    @FXML
    Button restartGameButton;
    
    @FXML
    Button ExitGameButton;
    
    PlayersManager playerManager;
    private SimpleBooleanProperty isMainMenuPressed = new SimpleBooleanProperty(false);
    private SimpleBooleanProperty isRestartPressed = new SimpleBooleanProperty(false);
    private SimpleBooleanProperty isExitPressed = new SimpleBooleanProperty(false);

    @FXML
    protected void onMainMenu(ActionEvent event) {
        isMainMenuPressed.set(true);
        isMainMenuPressed.set(false);
    }
    
    @FXML
    protected void onRestart(ActionEvent event) {
        isRestartPressed.set(true);
        isRestartPressed.set(false);
    }
    
    @FXML
    protected void onExit(ActionEvent event) {
        isExitPressed.set(true);
        isExitPressed.set(false);
    }
    
    protected SimpleBooleanProperty getMainMenuPressed() {
        return isMainMenuPressed;
    }
    
    protected SimpleBooleanProperty getRestertPressed() {
        return isRestartPressed;
    }
    
    protected SimpleBooleanProperty getExitPressed() {
        return isExitPressed;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        winnerNameLabel.setText(MainApp.clientGame.getWinnerPlayerName());
    }
}
