/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;



public class GameTimeOutViewController implements Initializable {

    @FXML
    Button mainMenuButton;

    @FXML
    Button ExitGameButton;

    private SimpleBooleanProperty isMainMenuPressed = new SimpleBooleanProperty(false);
    private SimpleBooleanProperty isExitPressed = new SimpleBooleanProperty(false);
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    protected void onMainMenu(ActionEvent event) {
        isMainMenuPressed.set(true);
        isMainMenuPressed.set(false);
    }
    
    @FXML
    protected void onExit(ActionEvent event) {
        isExitPressed.set(true);
        isExitPressed.set(false);
    }
    
    protected SimpleBooleanProperty getMainMenuPressed() {
        return isMainMenuPressed;
    }
        
    protected SimpleBooleanProperty getExitPressed() {
        return isExitPressed;
    }

    
}
